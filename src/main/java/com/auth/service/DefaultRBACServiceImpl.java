package com.auth.service;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class DefaultRBACServiceImpl implements RBACService{

	private final Logger logger = Logger.getLogger(DefaultRBACServiceImpl.class);
	
	final ObjectMapper mapper = new ObjectMapper(); 
	
	@Override
	public List<PermissionMatrix> parse(String data) {
		
		List<PermissionMatrix> permissionList = new ArrayList<>();
		
		try {
			String[] permissionDataArray = mapper.readValue(data, String[].class);
			boolean firstRecord = true;
			
			for(String dataValue : permissionDataArray) {
				if(firstRecord) {
					firstRecord = !firstRecord;
					continue;					
				}
				
				String[] content = dataValue.split(",");
				PermissionMatrix matrix = new PermissionMatrix(); 
				matrix.setRole(content[0]);
				List<String> permission = new ArrayList<>();
				for(int i = 1; i < content.length;i++) {
					permission.add(content[i].toLowerCase());
				}
				matrix.setPermission(permission);				
				permissionList.add(matrix);
			}
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage(), e);
		}
		
		return permissionList;
	}


}
