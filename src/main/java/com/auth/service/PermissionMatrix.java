package com.auth.service;

import java.util.List;

public class PermissionMatrix {
	String role;
	List<String> permission;
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public List<String> getPermission() {
		return permission;
	}
	public void setPermission(List<String> permission) {
		this.permission = permission;
	}
	
	
}
