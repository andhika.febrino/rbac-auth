package com.auth.service;

import java.util.List;

public interface RBACService {
	public List<PermissionMatrix> parse(String data);
}
