package com.auth.controller;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.jboss.logging.Logger;

import com.auth.service.DefaultRBACServiceImpl;
import com.auth.service.RBACService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.smallrye.reactive.messaging.annotations.Channel;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.kafka.Record;

public class AuthController {
	private final Logger logger = Logger.getLogger(AuthController.class);

	private final RBACService rbacService;
	
	final ObjectMapper mapper = new ObjectMapper(); 
	
	public AuthController(DefaultRBACServiceImpl rbacService) {
		this.rbacService = rbacService;
	}
	
    @Incoming("req-in")
    public void receive(Record<String, String> record) {
        logger.infof("Incoming : " + record.key() + " "+  record.value());
        try {
			logger.infof("Updating permission : " + mapper.writeValueAsString(rbacService.parse(record.value())));
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage(), e);
		}
    }
}
